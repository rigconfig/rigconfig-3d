const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const autoprefixer = require('autoprefixer');
const ReloadPlugin = require('reload-html-webpack-plugin');

module.exports = {
  entry: [
    'webpack/hot/dev-server',
    'webpack-dev-server/client?http://localhost:4000',
    'font-awesome-loader',
     path.join(__dirname, '/src/index.jsx')
  ],

  output: {
    path: path.join(__dirname, '/dist/a/'),
    filename: 'app.js',
  },

  devtool: '#cheap-module-eval-source-map',

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new ReloadPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, '/src/index.html'),
      filename: 'index.html',
      title: 'RigConfig 3D',
      inject: true,
    })
  ],

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader?sourceMap',
            options: {
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader'
          }
        ]
      },

      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader?sourceMap',
          },
          {
            loader: 'postcss-loader?sourceMap'
          },
          {
            loader: 'sass-loader?sourceMap'
          }
        ]
      },

      {
        test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'url-loader?limit=10000',
      },

      {
        test : /\.jsx?/,
        include : path.join(__dirname, '/src/'),
        loader : 'babel-loader'
      },

      {
        test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        use: 'file-loader',
      },

    ],
  },

  resolve: {
    extensions: ['*', '.js'],
    modules: [
      path.join(__dirname, "node_modules")
    ]
  }
};
