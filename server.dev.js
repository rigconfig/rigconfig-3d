/* eslint-disable no-console, import/no-extraneous-dependencies */
const path              = require('path');
const WebpackDevServer  = require('webpack-dev-server');
const express           = require('express');
const webpack           = require('webpack');
const config            = require('./webpack.dev.config');
const IP                = process.env.IP || 'localhost';
const PORT              = process.env.PORT || 4000;
// const projectRoot       = path.join(__dirname, '../../platforms/');
// const TARGET_PLATFORM   = process.env.TARGET_PLATFORM || 'html';

const compiler = webpack(config);
const server   = new WebpackDevServer(compiler, {
  hot: true,
  publicPath: config.output.publicPath,
  filename:   config.output.filename,
  historyApiFallback: true,
  stats: {
    colors: true,
    hash: false,
    version: false,
    chunks: false,
    children: false,
  }
});

server.use('/a', express.static(path.join('/src/a')));

server.listen(PORT, IP, err => {
  if (err) console.log(`=> OMG!!! 🙀 ${err}`);
  console.log(`=> 🔥  Webpack dev server is running on port ${PORT}`);
});
