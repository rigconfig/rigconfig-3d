const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: [
    'font-awesome-loader',
     path.join(__dirname, '/src/a/js/app.js'),
     path.join(__dirname, '/src/index.jsx')
  ],

  output: {
    path: path.join(__dirname, '/dist/a/'),
    filename: 'app.js',
  },

  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
    new ExtractTextPlugin({ filename: 'app.css', allChunks: true }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new CopyWebpackPlugin([
      {
        context: path.join(__dirname, '/src/a'),
        // from: '**/*',
        from: '**/*.+(html|css|js|gif|jpg|jpeg|png|svg|ttf|woff|woff2|eot)',
        to: path.join(__dirname, '/dist/a')
      }
    ]),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, '/src/index.html'),
      filename: '../index.html',
      title: 'RigConfig 3D - Prod',
      inject: true,
    })
  ],

  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader?minimize!postcss-loader',
        })
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader?minimize!postcss-loader!sass-loader'
        })
      },

      {
        test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'url-loader',
      },

      {
        test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        use: 'file-loader',
      },

      {
        test : /\.jsx?/,
        include : path.join(__dirname, '/src/'),
        loader : 'babel-loader'
      },
    ],
  },

  resolve: {
    extensions: ['*', '.js'],
    modules: [
      path.join(__dirname, "node_modules")
    ]
  }
};
