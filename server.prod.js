/* eslint-disable no-console, import/no-extraneous-dependencies */
const path            = require('path');
const express         = require('express');
const server          = express();
const bodyParser      = require('body-parser');
const IP              = process.env.IP || 'localhost';
const PORT            = process.env.PORT || 4000;
// const projectRoot     = path.join(__dirname, '../../platforms/');
// const TARGET_PLATFORM = process.env.TARGET_PLATFORM || 'html';

server.use([
  bodyParser.json(),
  bodyParser.urlencoded({ extended: true }),
  express.static(path.join(__dirname, '/dist'))
]);

server.listen(PORT, IP, err => {
  if (err) console.log(`=> OMG!!! 🙀 ${err}`);
  console.log(`=> 🚀  Production server is running on port ${PORT}`);
});
