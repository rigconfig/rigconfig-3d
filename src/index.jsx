import * as THREE from 'three'
import React from 'react';
import { render } from 'react-dom';
import AbstractApplication from './a/jsx/AbstractApplication.jsx';
import Main from './a/jsx/Main.jsx';
import ComponentSearch from './a/jsx/ComponentSearch.jsx';
import './a/css/app.scss';

class App extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <Main/>
        <ComponentSearch/>
      </div>
    );
  }
}

render(<App/>, document.getElementById('app'));
