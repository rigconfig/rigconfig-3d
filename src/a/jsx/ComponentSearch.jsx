import React from 'react';
import { render, setState } from 'react-dom';
import ComponentSearchInput from './ComponentSearchInput.jsx';
import Results from './Results.jsx';
import { Scrollbars } from 'react-custom-scrollbars';

class ComponentSearch extends React.Component {
  constructor() {
    super();
    this.clearResults = this.clearResults.bind(this);
    this.ShowResults = this.ShowResults.bind(this);
    this.HideResults = this.HideResults.bind(this);
    this.state = {
      results: [
        { name: "One", price: "99.00" },
        { name: "Two", price: "174.50" },
        { name: "Three", price: "35.00" },
        { name: "Four", price: "299.00" }
      ],
      counter: 5,
      ShowSearchComponents: true,
      ShowResults: false,
      HasResults: true
    }

    const resultsLoader = setInterval(() => {
        const newResults = this.state.results.concat([{name: this.state.counter.toString(), price: "5000.00"}])
        this.setState({ results: newResults, counter: this.state.counter + 1 })
        if (this.state.counter > 50) {
          clearInterval(resultsLoader);
          // console.log('Stopped!');
        }
      },
      30)
  }

  // Todo: reorganize components / container > header so that results scrollable wrapped dissapears on ESC
  //       maybe wrap <Scrollbars>?
  // Then the switching from scearch mode to db mode
  clearResults() {
    this.setState({results: [], ShowResults: false});
  }

  ShowResults() {
    this.setState({ShowResults: true});
  }

  HideResults() {
    this.setState({ShowResults: false, HasResults: false});
  }


  render() {
    var ShowSearchComponentsClass = this.state.ShowSearchComponents ? '' : 'closed' ;

    return (
      <div className="search-components">
        <div className="container">
          <div className="header">
            <a href="" onClick={(e) => {
              e.preventDefault();
              this.setState({ShowSearchComponents: !this.state.ShowSearchComponents});
            }}>
              <i className={"fa fa-bars " + ShowSearchComponentsClass}></i>
            </a>

            { this.state.ShowSearchComponents &&
              <span>
                <ComponentSearchInput
                  clearResults={this.clearResults}
                  ShowResults={this.ShowResults}
                  HideResults={this.HideResults}
                />
                <a href=""><i className="fa fa-search"></i></a>
                <a href=""><i className="fa fa-database"></i></a>
              </span>
            }
          </div>
        </div>

        {/* { this.state.HasResults && */}
          <Scrollbars
            style={ this.state.ShowSearchComponents ? { height: '100vh' } : {} }
            // autoHeight
            // autoHeightMin={calc(100vh)}
            // autoHeightMin="100%"
            // autoHeightMax="100%"
            autoHide
            autoHideTimeout={500}
            autoHideDuration={500}
            universal={true}
            // {...this.props}
            // results={this.state.results}
          >
            <Results results={this.state.results} />
          </Scrollbars>
        {/* } */}
      </div>
    )
  }
}

export default ComponentSearch;
