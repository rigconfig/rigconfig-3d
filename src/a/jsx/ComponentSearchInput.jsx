import React from 'react';
import { render } from 'react-dom';

class ComponentSearchInput extends React.Component {
  constructor() {
    super();
    this.handleEsc = this.handleEsc.bind(this);
    this.handleKeydown = this.handleKeydown.bind(this);
    this.textInput = null;
  }

  handleEsc(e) {
    if (e.keyCode === 27) { // ESC
      this.textInput.blur();
      this.textInput.value = '';
      this.props.clearResults();
    }
  }

  handleKeydown() {
    console.log('keydown');
  }

  componentDidMount() {
    this.textInput.addEventListener('keydown', this.handleEsc);
    this.textInput.addEventListener('keydown', this.handleKeydown);
    // console.log('Added event listener');
  }

  componentWillUnmount() {
    this.textInput.removeEventListener('keydown', this.handleEsc);
    this.textInput.removeEventListener('keydown', this.handleKeydown);
    // console.log('Removed event listener');
  }

  render() {
    return (
      <input
        type="text"
        className="search-components-input"
        ref={(input) => { this.textInput = input; }}
        onFocus={this.props.ShowResults}
        onBlur={this.props.HideResults}
        name="search-components-input"
        id="search-components-input"
        placeholder="Search for components..."
      />
    )
  }
}

export default ComponentSearchInput;
