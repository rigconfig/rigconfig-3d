import React from 'react';
import { render } from 'react-dom';

const Results = ({results}) => {
  return (
    <div className="results">
      { results.map((v, i) => { return (<div className="item" key={v.name.split(' ').join('')}>{v.name} - {v.price}</div>) }) }
    </div>
  )
}

export default Results
