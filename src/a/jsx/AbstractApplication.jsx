import React from 'react';
import { render } from 'react-dom';
import * as THREE from 'three'
import '../js/DragControls.js'
import '../js/OrbitControls.js'

class AbstractApplication extends React.Component {
  constructor() {
    super();

/*
    this._camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 1000 );
    this._camera.position.z = 400;

    this._scene = new THREE.Scene();

    this._renderer = new THREE.WebGLRenderer();
    this._renderer.setPixelRatio( window.devicePixelRatio );
    this._renderer.setSize( window.innerWidth, window.innerHeight );

    this._three_container = document.getElementById('app');
    this._three_container.appendChild( this._renderer.domElement );
    // document.body.appendChild( this._renderer.domElement );

    this._controls = new THREE.OrbitControls( this._camera, this._renderer.domElement );
    //this._controls.addEventListener( 'change', render ); // add this only if there is no animation loop (requestAnimationFrame)
    // this._controls.enableDamping = true;
    // this._controls.dampingFactor = 0.25;
    // this._controls.enableZoom = false;

    window.addEventListener( 'resize', this.onWindowResize.bind(this), false );
    */

    this._settings = {
      controls: true,
      dragControls: true
    };

    // this._controls;
    // this._dragControls;
    // this._renderer = new THREE.WebGLRenderer({canvas: document.querySelector("canvas")});
    this._renderer = new THREE.WebGLRenderer();
    // this._renderer.setPixelRatio( window.devicePixelRatio );
    // this._renderer.setSize( window.innerWidth, window.innerHeight );

    // There's no reason to set the aspect here because we're going
    // to set it every frame any
    this._camera = new THREE.PerspectiveCamera(70, 1, 1, 1000);
    this._camera.position.z = 400;

    if (this._settings.controls) {
      this._controls = new THREE.OrbitControls( this._camera, this._renderer.domElement );
      // console.log('true...');
      // console.log(this._controls);
    }

    this._scene = new THREE.Scene();
    this._geometry = new THREE.BoxGeometry(200, 200, 200);
    this._material = new THREE.MeshPhongMaterial({
      color: 0x555555,
      specular: 0xffffff,
      shininess: 50,
      flatShading: THREE.SmoothShading
    });

    this._mesh = new THREE.Mesh(this._geometry, this._material);
    this._scene.add(this._mesh);

    if (this._settings.dragControls) {
      let objects = [];
      objects.push(this._mesh);
      this._dragControls = new THREE.DragControls( objects, this._camera, this._renderer.domElement );
    }

    if (this._settings.dragControls && this._settings.controls) {
      this._dragControls.addEventListener( 'dragstart',  event => this._controls.enabled = false );
      this._dragControls.addEventListener( 'dragend', event => this._controls.enabled = true );
    }

    // const light1 = new THREE.PointLight(0xff80C0, 2, 0);
    let light1 = new THREE.PointLight(0xed1212, 3, 0, 2);
    light1.position.set(200, 100, 10000);
    this._scene.add(light1);

    // scene.background = new THREE.Color( 0xffffff );
    // scene.background = new THREE.Color( 0xebebeb );
    this._scene.background = new THREE.Color( 0xf2f2f2 );

    this._mesh.rotation.x = 0.5;
    this._mesh.rotation.y = 0.5;

    this._three_container = document.getElementById('main-canvas');
    this._three_container.appendChild( this._renderer.domElement );

    window.addEventListener( 'resize', this.onWindowResize.bind(this), false );
    // requestAnimationFrame(animate);
    // this.animate.bind(this);
    // this.animate();
    this._renderer.render(this._scene, this._camera);
    // this.animate();
    // requestAnimationFrame();
    requestAnimationFrame(this.animate.bind(this));
  }

  get renderer(){

    return this._renderer;

  }

  get camera(){

    return this._camera;

  }

  get scene(){

    return this._scene;

  }

  onWindowResize() {
    this.resizeCanvasToDisplaySize();
  }

  resizeCanvasToDisplaySize(force) {
    // this._camera.aspect = window.innerWidth / window.innerHeight;
    // this._camera.updateProjectionMatrix();
    // this._renderer.setSize( window.innerWidth, window.innerHeight );
    const canvas = this._renderer.domElement;
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    if (force || canvas.width !== width || canvas.height !== height) {
      // you must pass false here or three.js sadly fights the browser
      this._renderer.setSize(width, height, false);
      // renderer..setClearColor(0xffffff, 0); // sm
      this._camera.aspect = width / height;
      this._camera.updateProjectionMatrix();

      // set render target sizes here
    }

    // const canvas = renderer.domElement;
    // const width = canvas.clientWidth;
    // const height = canvas.clientHeight;
    // if (force || canvas.width !== width || canvas.height !== height) {
    //   // you must pass false here or three.js sadly fights the browser
    //   renderer.setSize(width, height, false);
    //   // renderer..setClearColor(0xffffff, 0); // sm
    //   camera.aspect = width / height;
    //   camera.updateProjectionMatrix();
    //
    //   // set render target sizes here
    // }
  }

  animate(timestamp) {
    // OLD
    // requestAnimationFrame( this.animate.bind(this) );
    // //this._controls.update();
    // this._renderer.render( this._scene, this._camera );

    timestamp *= 0.001;  // seconds
    this.resizeCanvasToDisplaySize();
    this._mesh.rotation.x = timestamp * 0.5;
    this._mesh.rotation.y = timestamp * 1;

    requestAnimationFrame(this.animate.bind(this));

    this._renderer.render(this._scene, this._camera);

    // console.log('animate!');

    // if (settings.controls) {
    //   controls.update();
    // }


  }
}

export default AbstractApplication;
